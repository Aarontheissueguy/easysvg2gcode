# Define the commands to keep
import os
import subprocess

### EDIT THESE VALUES TO FIT YOUR MACHINE
feed_rate = " " + "F1200" #Drawing speed
start_gcode = 'G21\nG90\nG28\n'
end_gcode = 'G28\n'
pen_down_gcode = 'G0 Z8. ; Pen down\n'
pen_up_gcode = 'G0 Z14. ; Pen up\n'
x_offset = 0
y_offset = 20
### EDIT THESE VALUES TO FIT YOUR MACHINE


keep_commands = ['G1', 'G0', 'M4', 'M5']
input_file_path = 'input.gcode'
output_file_path = 'output.gcode'
def convert_svg_to_gcode():
    input_file = "Input/" + os.listdir("Input")[0]
    output_file = "input.gcode"
    try:
        subprocess.run(["svg2gcode", "--nofill",input_file, output_file], check=True)
        print("Conversion successful!")
    except subprocess.CalledProcessError as e:
        print(f"Conversion failed with error: {e}")

def filter_gcode(keep_commands, feed_rate, M4_replacement, M5_replacement, start_gcode, end_gcode, input_file_path, output_file_path):
    with open(input_file_path, 'r') as input_file:
        # Read lines from the input file
        lines = input_file.readlines()

    # Filter lines containing the desired commands and replace M4 and M5
    filtered_lines = []
    for line in lines:
        if any(command in line for command in keep_commands):
            if 'M4' in line:
                filtered_lines.append(M4_replacement)
            elif 'M5' in line:
                filtered_lines.append(M5_replacement)
            elif 'G1' in line:
                line_without_feed_and_spindle = ' '.join(part for part in line.strip().split() if not (part.startswith('F') or part.startswith('S')))
                filtered_lines.append(line_without_feed_and_spindle + feed_rate + '\n')
            else:
                filtered_lines.append(line + "\n")

    # Add start and end G-code
    filtered_lines.insert(0, start_gcode)
    filtered_lines.append(end_gcode)

    # Write filtered lines to the output file
    with open(output_file_path, 'w') as output_file:
        output_file.writelines(filtered_lines)
    output_file.close()
    print("Filtered G-code written to:", output_file_path)

def apply_offset_to_gcode(input_file, output_file, x_offset, y_offset):
    with open(input_file, 'r') as f:
        lines = f.readlines()

    with open(output_file, 'w') as f:
        for line in lines:
            if line.startswith('G1') or line.startswith('G0'):  # Assuming coordinates are in G1 or G0 commands
                # Split the line into individual components
                components = line.split(' ')
                modified_components = []

                for component in components:
                    if component.startswith('X'):
                        x_y_values = component[1:].split('Y')  # Split X and Y values
                        x_value = float(x_y_values[0])
                        x_value += x_offset
                        modified_components.append('X' + str(x_value))
                        if len(x_y_values) > 1:  # If there's a Y value present
                            modified_components.append('Y' + x_y_values[1])  # Append the Y value as it is
                    elif component.startswith('Y'):
                        y_value = float(component[1:])
                        y_value += y_offset
                        modified_components.append('Y' + str(y_value))
                    else:
                        modified_components.append(component)

                # Write the modified line to the output file
                f.write(' '.join(modified_components))
            else:
                # Write non-coordinate lines unchanged
                f.write(line)

    print("Offset applied. G-code written to:", output_file_path)

convert_svg_to_gcode()
filter_gcode(keep_commands, feed_rate, pen_down_gcode, pen_up_gcode, start_gcode, end_gcode, input_file_path, output_file_path)
apply_offset_to_gcode(output_file_path, "output.gcode", x_offset, y_offset)
os.remove("input.gcode")
